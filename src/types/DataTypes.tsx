
export type PostType = {
    id : number,
    text: string,
    likes: number
}

export type MessageType = "SUCCESS" | "ERROR" | "NONE"