import React from 'react';
import { PostPage } from './pages/PostPage';

const App: React.FC = () => {
  return (
    <>
      <PostPage />
    </>
  );
}

export default App;
