import * as React from "react"
import { Header } from "../components/Header";
import { Footer } from "../components/Footer";
import "./PostPage.css"
import { Content } from "../components/Content";

type Props = {
}


class PostPage extends React.PureComponent<Props, {}> {


    render() {
        return(
            <div className="wrappers">
                <Header style="header"  title="Post Segware =)" />

                <Content style="content" />

                <Footer style="footer" author="Junior Nakamura" email="jnakamura@gmail.com" />


            </div>
        )
    }


}

export { PostPage }