import * as React from 'react'
import { PostType } from '../types/DataTypes';
import { Card, CardBody, CardText, Button, CardFooter } from 'reactstrap';
import "../components/ComponentStyle/CardStyle.css"
type Props = {
    post: PostType,
    likeEvent: (Post : PostType) => void

}

class CardComponent extends React.PureComponent<Props, {}> {


    render() {
        const { post, likeEvent } = this.props 

        return (
            <div>
                <Card key= {post.id}>
                    <CardBody>
                        <CardText> { post.text }  </CardText>
                    </CardBody>
                    <CardFooter className="card-footer">
                        <Button color="success"  onClick={() => likeEvent(post)}>Like</Button>
                        { post.likes > 0 && <Button color="danger" className="no-clickable" > { `${post.likes} likes`  } </Button>}
                        { post.likes === 0 && <Button color="secondary" className="no-clickable" > No likes yet </Button>}
                    </CardFooter>
                </Card>
            </div>              

        )


    }

}

export { CardComponent }