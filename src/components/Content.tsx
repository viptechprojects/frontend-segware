import * as React from 'react';
import { Alert } from 'reactstrap';
import { PostComponent } from './PostComponent';
import * as axios from 'axios'
import { PostType, MessageType } from '../types/DataTypes';
import { ListPostComponent } from './ListPostComponent';


type Props = {
    style: string
}

type State = {
    listOfPost: PostType[],
    alertType: MessageType ,
    alertText: string
}

class Content extends React.PureComponent<Props, State> {

    state : State = {
        listOfPost: [],
        alertType: "NONE",
        alertText: ""
    }

    componentWillMount() {
        this.getPosts()
    }

    onDismiss = () => {
        this.setState({ 
            alertType: "NONE"
        })
    }

    renderMessage() {
        const { alertType, alertText } = this.state

        return (
            <div>
                <br />
                <Alert
                 color={alertType === "SUCCESS"? "primary" : "danger"}
                 isOpen={alertType !== "NONE"}
                 toggle={this.onDismiss} 
                 fade={false}>
                    { alertText }
                </Alert>
            </div>
        )            
    }

    newLike = (post: PostType) => {
        axios.default.put(`http://localhost:8080/posts/${post.id}/like/add`)
        .then(response => {
            this.setState(state => ({
                ...state,
                alertType: "NONE",
                alertText: ""

            }))

            this.getPosts()

        })
        .catch(error => {
            this.setState(state => ({
                ...state,
                alertType: "ERROR",
                alertText: error.message

            }))
        })

    }

    submitPost = (post: PostType) => {
        axios.default.post('http://localhost:8080/posts', post)
        .then(response => {
            this.setState(state => ({
                ...state,
                alertType: "SUCCESS",
                alertText: "The Post was created"

            }))

            this.getPosts()
        })
        .catch(error => {
            this.setState(state => ({
                ...state,
                alertType: "ERROR",
                alertText: error.message

            }))
        })

    }

    getPosts = () => {
        axios.default.get('http://localhost:8080/posts')
        .then(response => {
            this.setState(state => ({
                ...state,
                listOfPost: response.data as PostType[]
            }))
        })
        .catch(error => {
            this.setState(state => ({
                ...state,
                alertType: "ERROR",
                alertText: error.message

            }))

        })

    }


    render() {
        const { style } = this.props
        const { listOfPost, alertType } = this.state

        return (
            <div className={style}>

                <PostComponent submitEvent={this.submitPost} />

                {alertType === "NONE" && <ListPostComponent likeEvent={this.newLike} listOfPosts={listOfPost} /> }       

                { alertType !== "NONE" && this.renderMessage() }       

            </div>
        )

    }

}

export { Content }