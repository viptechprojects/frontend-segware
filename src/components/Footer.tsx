import * as React from 'react'

type Props = {
    author: string,
    email: string,
    style: string

}

class Footer extends React.PureComponent<Props, {}> {

    render() {
        const { author, email, style } = this.props

        return (

            <div className={style}>
                <p> Developed by {`${author} - ${email}`} </p>
            </div>

        )

    }

}

export { Footer }