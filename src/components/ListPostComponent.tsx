import * as React from 'react'
import { PostType } from '../types/DataTypes';
import { CardComponent } from './CardComponent';
import "./ComponentStyle/CardStyle.css"
type Props = {
    listOfPosts: PostType[]
    likeEvent: (Post : PostType) => void
}


class ListPostComponent extends React.PureComponent<Props, {}> {

    renderList(listOfPosts: PostType[]) {
        const { likeEvent } = this.props

        return listOfPosts.map(post => {

            return (
                <div className="card-display">
                    <CardComponent likeEvent={likeEvent} post={post} />
                </div>
            )

        })


    }

    render() {

        const { listOfPosts } = this.props

        return (
            <div className="card-container">
                {this.renderList(listOfPosts)}
            </div> 

        )
    }

}

export { ListPostComponent }