import * as React from 'react'
import { Row, Col, Form, FormGroup, Label, Input, Button } from 'reactstrap';
import { PostType } from '../types/DataTypes';

type Props = {
    submitEvent: (post: PostType) => void

}

type State = {
    post: PostType

}

class PostComponent extends React.PureComponent<Props, State> {

    state : State = {
        post: {
            id: 0,
            text: "",
            likes: 0
        }

    }

    defaultPost = () => {
        return {
            id: 0,
            text: "",
            likes: 0
        } as PostType
    }

    submitPost = (event: React.MouseEvent<any, MouseEvent>) => {
        const { submitEvent } = this.props
        const { post } = this.state

        event.preventDefault()

        submitEvent(post)

        this.setState(state => ({
            post: this.defaultPost()
        }))

    }

    handlePost = (event : React.ChangeEvent<HTMLInputElement>) => {
        event.persist()
        this.setState(state => ({
            post: {
                ...state.post,
                text: event.target.value,
            }

        }))


    }

    render() {
        const { post } = this.state

        return (
            <div>
                <Row>
                    <Col lg="3" md="2" />
                    <Col lg="6" md="8" xs="12">
                        <Form>
                            <FormGroup>
                                <Label for="postText">Type something</Label>
                                <Input type="textarea" name="text" id="postText" value={post.text} onChange = { this.handlePost } />
                            </FormGroup>
                            <Button color="primary" size="lg" block onClick={this.submitPost}>Submit</Button>
                        </Form>
                    </Col>
                    <Col lg="3" md="2" />
                </Row>
            </div>

        )
    }

}

export { PostComponent }