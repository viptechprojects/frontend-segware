import * as React from 'react';

type Props = {
    title: string,
    style: string
}

type State = {

}

class Header extends React.PureComponent<Props, State> {

    render() {
        const { title, style } = this.props


        return (
            <div className = {style}>
                <h1> { title } </h1>
            </div>


        )


    }


}

export { Header }